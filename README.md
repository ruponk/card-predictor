# Card Predictor

## Description

This is a repository that contains links to 2 other repositories that contain the microservices that work together to form a card predictor game.  
There is also a docker-compose configuration in this that will build and launch the microservices along with an instance of the mysql database.

## Installation

To install the whole setup, follow these steps:  

1. Clone this repository
2. Enter into the folder that was created for the repository
3. Run the following command in the folder `git submodule update --init`
4. That's it, you've got the whole thing

## Running

To run the microservices, simply run `docker-compose up` in the folder of the repository. Make sure to have docker and docker-compose installed.  
There are 2 users in `wallet/src/sequelize/seeders/user.ts` that can be used. If they're not in the database, they can be placed inside it by running `npm run sequelize:seed` inside the directory of the `wallet` microservice after all the node packages have been installed for the microservice.  
The `sequelize:seed` npm script relies on the configuration inside `config/default.js` just like any other script.

## Setting up the provided mysql instance

There are some npm scripts in each of the microservice for setting up the provided instance of mysql.  
They can be used after the database is started.  

To use these scripts, follow these steps:  

1. Enter the directory of the microservice
2. Run `npm ci` to install the packages that are required by the npm scripts
3. Run `npm run sequelize:setup` to create the database, the tables and some initial data in them

There are specialized npm scripts for each of the steps in the setup, so use those if the whole setup does not work.  
The npm scripts, as well as the microservices, depend on the configuration found in a file named `default.js` found in a folder named `config` inside the directory of the microservice.

## Rest API

Both microservices use a rest api to communicate between themselves and the rest of the world.  
A simple description of the api can be found in the [postman](https://www.postman.com/) collections placed inside the `postman-collections` folder inside this repository.